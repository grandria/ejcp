# Example with a make-based project: NPB

If you are using the docker image, the NAS Parallel Benchmarks are installed in `$HOME/NPB3.4.2`, and the default `make.def` has already been created in `$HOME/NPB3.4.2/NPB3.4-MPI/config`.
If you are not using the docker image, please extract the NPB and create the `make.def` file from the included `make.def.template`.

By default the makefile uses mpicc, therefore to run the analysis in such a setup the idea is to leverage the `OMPI_CC` variable to run `parcoachcc`, which can be done with the following commands:
```bash
cd $HOME/NPB3.4.2/NPB3.4-MPI
OMPI_CC="parcoachcc clang-15" make is CLASS=S CC=clang-15
```

In the build output you should see a set of warnings:
```bash
PARCOACH: is.c: warning: MPI_Finalize line 1001 possibly not called by all processes because of conditional(s) line(s)  1000 (is.c) (Call Ordering Error)
PARCOACH: is.c: warning: MPI_Alltoall line 758 possibly not called by all processes because of conditional(s) line(s)  1000 (is.c) (Call Ordering Error)
PARCOACH: is.c: warning: MPI_Alltoallv line 773 possibly not called by all processes because of conditional(s) line(s)  1000 (is.c) (Call Ordering Error)
PARCOACH: is.c: warning: MPI_Reduce line 1186 possibly not called by all processes because of conditional(s) line(s)  1000 (is.c) (Call Ordering Error)
PARCOACH: is.c: warning: MPI_Allreduce line 696 possibly not called by all processes because of conditional(s) line(s)  1000 (is.c) (Call Ordering Error)
PARCOACH: is.c: warning: MPI_Bcast line 1055 possibly not called by all processes because of conditional(s) line(s)  1000 (is.c) (Call Ordering Error)
PARCOACH: is.c: warning: MPI_Finalize line 1213 possibly not called by all processes because of conditional(s) line(s)  1000 (is.c) (Call Ordering Error)
PARCOACH: is.c: warning: MPI_Reduce line 1172 possibly not called by all processes because of conditional(s) line(s)  1000 (is.c) (Call Ordering Error)
PARCOACH: is.c: warning: MPI_Reduce line 1179 possibly not called by all processes because of conditional(s) line(s)  1000 (is.c) (Call Ordering Error)
PARCOACH: is.c: warning: MPI_Reduce line 1108 possibly not called by all processes because of conditional(s) line(s)  1000 (is.c) (Call Ordering Error)
PARCOACH: is.c: warning: MPI_Reduce line 1124 possibly not called by all processes because of conditional(s) line(s)  1000 (is.c) (Call Ordering Error)
```

If you just want to run PARCOACH that's basically it, now if you want to actually instrument and run the code, we have let this as an exercise.

Based on the information given in [example-manual](../example-manual), you should be able to adjust the value of the `OMPI_CC` variable, as well to change the `IS/Makefile` in order to link the instrumented library.

Please be aware that `parcoachcc` needs to be explicitly aware of the final object file, therefore you need to change these lines in the `Makefile`:
```make
is.o:             is.c  npbparams.h
```

To this (notice the `-o is.o`):
```make
is.o:             is.c  npbparams.h
	${CCOMPILE} $< -o is.o
```

If you want to see the solution and the reasoning you can take a look at [SolutionInstr.md](./SolutionInstr.md).
