This is a simple pass example to illustrate how creating an out-of-tree pass works, with an example slightly more complicated than a HelloWorld.


## Build

It should be easy enough to simply use a default cmake invocation:

```bash
mkdir build && cd build
cmake ..
make -j
```

## Running the thing

Get into the `build` folder.
The cmake creates an executable `opt_with_plugin` which automatically load the plugin;
you can use it to run the pass over `inputs/add_sub.ll` using either:
```bash
./opt_with_plugin -S -O1 ../inputs/add_sub.ll
```
(the pass is registered at the beginning of any optimization pipeline)

Or by invoking the pass on its own:
```bash
./opt_with_plugin -S -passes=add2sub ../inputs/add_sub.ll
```

You may also want to try the Hello pass by running it through opt:

```bash
./opt_with_plugin -S -passes=hello ../inputs/add_sub.ll
```

