# Example with CMake

We create a CMake package to make it easy for user to integrate PARCOACH in their CMake project.

We can find PARCOACH using CMake's `find_package`:
```cmake
# The default 'COMPONENTS' looked for is Analysis, populating the PARCOACH_BIN
# and PARCOACHCC_BIN variables.
# In order to instrument a target we need to ask for the instrumentation library.
find_package(Parcoach REQUIRED COMPONENTS Instrumentation_C)
```

From here we can use a couple of helpers on CMake targets:
  - `parcoach_coll_c_instrument`, to instrument MPI collectives on a given C program.
  - `parcoach_rma_c_instrument`, to instrument MPI RMA operations on a given C program.
  - `parcoach_rma_fortran_instrument`, to instrument MPI RMA operations on a given Fortran program (not covered here).

Instrumenting the code is as simple as calling the helper on the target; here we create a list of source files, then for each of them we add an executable, link MPI, and tell parcoach to instrument it:

```cmake
set(DEMO_COLLECTIVES
  MPIexample
  mismatch_barrier
  )

foreach(PROG ${DEMO_COLLECTIVES})
  add_executable(${PROG} ${PROG}.c)
  target_link_libraries(${PROG} MPI::MPI_C)
  parcoach_coll_c_instrument(${PROG})
endforeach()
```

In the docker image, assuming we're in the home directory (`cd $HOME` to make sure), we can configure and build the project this way:
```bash
$ mkdir build
$ cd build/
$ cmake ../sources/example-cmake/
-- The C compiler identification is Clang 15.0.7
-- The CXX compiler identification is Clang 15.0.7
-- Found MPI: TRUE (found version "3.1") found components: C 
-- Using mpicc: /usr/bin/mpicc
-- Parcoach version 2.4.2 found in /usr/lib/cmake/Parcoach.
-- Parcoach: instrumenting MPIexample with launcher /home/ubuntu/build/MPIexample.launcher.
-- Parcoach: instrumenting mismatch_barrier with launcher /home/ubuntu/build/mismatch_barrier.launcher.
-- Parcoach: instrumenting ll_get_get_inwindow with launcher /home/ubuntu/build/ll_get_get_inwindow.launcher.
-- Configuring done
-- Generating done
-- Build files have been written to: /home/ubuntu/build
```
The output from CMake has been cut a bit but it should look like that, and tell us what compiler is being used (it should be Clang!), where MPI has been found, and where PARCOACH has been found.

Please run `make` to build the programs, its output will include the warnings found by PARCOACH on each file; here is an excerpt from the output:
```bash
$ make
[ 16%] Building C object CMakeFiles/MPIexample.dir/MPIexample.c.o
+ /usr/bin/parcoachcc -instrum-inter --args /usr/bin/cc -isystem /usr/lib/x86_64-linux-gnu/openmpi/include -isystem /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi -MD -MT CMakeFiles/MPIexample.dir/MPIexample.c.o -MF CMakeFiles/MPIexample.dir/MPIexample.c.o.d -o CMakeFiles/MPIexample.dir/MPIexample.c.o -c /home/ubuntu/sources/example-cmake/MPIexample.c
PARCOACH: sources/example-cmake/MPIexample.c: warning: MPI_Reduce line 10 possibly not called by all processes because of conditional(s) line(s)  24 (sources/example-cmake/MPIexample.c) (Call Ordering Error)
[ 50%] Building C object CMakeFiles/mismatch_barrier.dir/mismatch_barrier.c.o
+ /usr/bin/parcoachcc -instrum-inter --args /usr/bin/cc -isystem /usr/lib/x86_64-linux-gnu/openmpi/include -isystem /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi -MD -MT CMakeFiles/mismatch_barrier.dir/mismatch_barrier.c.o -MF CMakeFiles/mismatch_barrier.dir/mismatch_barrier.c.o.d -o CMakeFiles/mismatch_barrier.dir/mismatch_barrier.c.o -c /home/ubuntu/sources/example-cmake/mismatch_barrier.c
PARCOACH: sources/example-cmake/mismatch_barrier.c: warning: MPI_Barrier line 19 possibly not called by all processes because of conditional(s) line(s)  18 (sources/example-cmake/mismatch_barrier.c) (Call Ordering Error)
[ 83%] Building C object CMakeFiles/ll_get_get_inwindow.dir/ll_get_get_inwindow.c.o
+ /usr/bin/parcoachcc -check=rma --args /usr/bin/cc -isystem /usr/lib/x86_64-linux-gnu/openmpi/include -isystem /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi -MD -MT CMakeFiles/ll_get_get_inwindow.dir/ll_get_get_inwindow.c.o -MF CMakeFiles/ll_get_get_inwindow.dir/ll_get_get_inwindow.c.o.d -o CMakeFiles/ll_get_get_inwindow.dir/ll_get_get_inwindow.c.o -c /home/ubuntu/sources/example-cmake/ll_get_get_inwindow.c
===========================
===  PARCOACH ANALYSIS  ===
===========================
===========================
ANALYZING function main...
(1) Get statistics ...done 
(2) Local concurrency errors detection ...LocalConcurrency detected: conflit with the following instructions: 
  %33 = call i32 @MPI_Get(ptr noundef %31, i32 noundef 1, ptr noundef @ompi_mpi_int, i32 noundef 1, i64 noundef 25, i32 noundef 1, ptr noundef @ompi_mpi_int, ptr noundef %32), !dbg !97 - LINE 38 in sources/example-cmake/ll_get_get_inwindow.c
AND
  %30 = call i32 @MPI_Get(ptr noundef %28, i32 noundef 1, ptr noundef @ompi_mpi_int, i32 noundef 1, i64 noundef 20, i32 noundef 1, ptr noundef @ompi_mpi_int, ptr noundef %29), !dbg !94 - LINE 35 in sources/example-cmake/ll_get_get_inwindow.c
done 
(3) Instrumentation for dynamic analysis ...done 
=== STATISTICS === 
11 MPI functions including 6 RMA functions 
= WINDOW CREATION/DESTRUCTION: 1 MPI_Win_free, 1 MPI_Win_create 
= EPOCH CREATION/DESTRUCTION: 0 MPI_Win_fence, 0 MPI_Lock, 1 MPI_Lockall 0 MPI_Unlock, 1 MPI_Unlockall 
= ONE-SIDED COMMUNICATIONS: 2 MPI_Get, 0 MPI_Put, 0 MPI_Accumulate 
= SYNCHRONIZATION: 0 MPI_Win_Flush 
LOAD/STORE STATISTICS: 4 (/7) LOAD and 0 (/4) STORE are instrumented
===========================
```

Now if we want to run one of the programs we can just use:
```bash
mpiexec -n 2 ./MPIexample
```

As an exercise for the reader: some examples include false positives, which means some warnings are not actually triggered at runtime, and some are triggered in specific setups.
Try to read the C files, understand where the warnings are coming from, and if/when they would trigger at runtime.
Then you can verify your guess by actually running the programs with the different parameters.

Now that we have seen a CMake example, let's see how we can integrate PARCOACH in a purely make-based program: the [NAS Parallel Benchmarks](../example-nas).
