# Manual example with parcoach

The overall process to run parcoach over a given program would be the following:
  - Generate an LLVM Intermediate Representation (IR) of the program
  - Run parcoach over the IR (optionally tell parcoach to instrument the code)
  - Optionally compile the instrumented IR to an object file
  - Link everything together with parcoach's instrumentation library

It can be a very annoying process to do manually, therefore we introduced a wrapper, `parcoachcc`, which can be prefixed to an `mpicc` invocation to automatically perform the analysis/instrumentation in addition to generating the original object.

Let's take an example with the file `mismatch_barrier.c` in this folder.
On would generate an object file like this:
```bash
mpicc mismatch_barrier.c -c -o mismatch_barrier.o
```

Since the default compiler for the `mpicc` we are using is gcc, we need to tell OpenMPI to use clang instead of gcc so that `mpicc` can generate LLVM IR:
```bash
export OMPI_CC=clang-15
```

Then we can simply run the previous line and prefix it with `parcoachcc`:
```bash
$ parcoachcc mpicc mismatch_barrier.c -c -o mismatch_barrier.o
remark: Parcoach: running '/usr/bin/mpicc mismatch_barrier.c -c -o mismatch_barrier.o'
remark: Parcoach: running '/usr/bin/mpicc mismatch_barrier.c -g -S -emit-llvm -o parcoach-ir-9cf6fd.ll'
remark: Parcoach: running '/usr/bin/parcoach parcoach-ir-9cf6fd.ll'
PARCOACH: mismatch_barrier.c: warning: MPI_Barrier line 19 possibly not called by all processes because of conditional(s) line(s)  18 (mismatch_barrier.c) (Call Ordering Error)
```
As we can see it ran the original command, then generate the LLVM IR, then ran parcoach over it and displayed the one warning.

Now let's assume we want to instrument the code and run it, we need to tell parcoach to do it; it uses the same behavior as `gdb`: you can pass arguments to `parcoachcc`, then pass `--args` and all arguments after that will be the program to run.
We end up with this invocation:

```bash
$ parcoachcc -instrum-inter --args mpicc mismatch_barrier.c -c -o mismatch_barrier.o
remark: Parcoach: running '/usr/bin/mpicc mismatch_barrier.c -c -o mismatch_barrier.o'
remark: Parcoach: running '/usr/bin/mpicc mismatch_barrier.c -g -S -emit-llvm -o parcoach-ir-e93166.ll'
remark: Parcoach: running '/usr/bin/parcoach -instrum-inter parcoach-ir-e93166.ll -o parcoach-ir-939f91.ll'
PARCOACH: mismatch_barrier.c: warning: MPI_Barrier line 19 possibly not called by all processes because of conditional(s) line(s)  18 (mismatch_barrier.c) (Call Ordering Error)
remark: Parcoach: running '/usr/bin/mpicc parcoach-ir-939f91.ll -c -o mismatch_barrier.o'
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include' [-Wunused-command-line-argument]
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi' [-Wunused-command-line-argument]
```

There is now an additional execution after parcoach: it generates the instrumented object file at the end.

We can now generate the final binary and link the instrumentation library:

```bash
mpicc mismatch_barrier.o -lParcoachCollDynamic_MPI_C
```

And run it:
```bash
$ mpiexec -n 2 ./a.out
PARCOACH DYNAMIC-CHECK : Error detected on rank 0
PARCOACH DYNAMIC-CHECK : Abort is invoking line 21 before calling MPI_Finalize in mismatch_barrier.c
PARCOACH DYNAMIC-CHECK : see warnings  
--------------------------------------------------------------------------
MPI_ABORT was invoked on rank 0 in communicator MPI_COMM_WORLD
```

Indeed a deadlock was detected at runtime by PARCOACH, and the warning is correct.
If we take a look at the original code we can see that the `MPI_Barrier` is not called by all processes.

The instrumentation process can be a bit annoying if the project is bigger than a single file, therefore we have introduced CMake fonctions to do that for you!
Checkout the [CMake examples](../example-cmake) for the next step of the tutorial.
